$(function() {



$('.sidebar-catalog .drop > a').click(function() {
  $(this).parent().toggleClass('active');
  $(this).next().fadeToggle()
})



// FansyBox
 $('.fancybox').fancybox({});

// Стилизация селектов
$('select').styler();


// Begin of scroll nav
$(".scroll").click(function(event) {
  //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 20}, 1500);
}); 
// End of scroll nav


$('.services-more__header').click(function() {
  $(this).toggleClass('active')
  $(this).parent().find('.services-more__body').slideToggle()
})


// Слайдер продуктов
$('.leaders-sales__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 580,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});


// Слайдер партнерев
$('.partner-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 350,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});

// Слайдер клиентов
$('.customer-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: true,
    adaptiveHeight: true,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 350,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});





//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});



$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
  });



// слайдер карточки товара
var productCartSlider = $('.product-cart__slider').slick({
    slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
    asNavFor: '.product-cart__slider--nav'
});

$('.product-cart__slider--next').click(function(){
    $(productCartSlider).slick("slickNext")
});

$('.product-cart__slider--nav').slick({
    slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.product-cart__slider',
  dots: false,
  arrows: false,
  focusOnSelect: true
});



// 
$('.show-more__btn').click(function() {
  $(this).toggleClass('active');
  $(this).parent().toggleClass('active')
  $(this).parent().find('.show-more__body').fadeToggle()
})



// слайдер лицензий
$('.licenses-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.licenses-slider__nav',
    adaptiveHeight: true
});


$('.licenses-slider__nav').slick({
    slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.licenses-slider',
  dots: false,
  focusOnSelect: true
});

// BEGIN of script for header submenu
$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
});


// слайдер проектов
var projectsSlider = $('.projects-slider').slick({
    slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      adaptiveHeight: true
});
$('.projects-slider__next').click(function(){
    $(projectsSlider).slick("slickNext")
});
$('.projects-slider__prev').click(function(){
    $(projectsSlider).slick("slickPrev")
});


// Сайдбар на небольших экранах
$('.sidebar-btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})

$('.sidebar-close').click(function() {
  $('.sidebar').toggleClass('active')
})


// Логин на мобильных экранах
$('.header-login').click(function() {
  $(this).children('.header-login__wrap').fadeToggle();
})

})